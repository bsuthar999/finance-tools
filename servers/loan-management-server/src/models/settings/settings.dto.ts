import { IsUrl, IsNotEmpty, IsOptional } from 'class-validator';
// import { ApiModelProperty } from '@nestjs/swagger';

export class SettingsDto {
  uuid?: string;

  @IsUrl()
  @IsNotEmpty()
  appURL: string;

  @IsUrl()
  authServerURL: string;

  @IsNotEmpty()
  clientId: string;

  @IsNotEmpty()
  clientSecret: string;

  @IsUrl({ allow_underscores: true }, { each: true })
  callbackURLs: string[];

  @IsOptional()
  type: string;
}
