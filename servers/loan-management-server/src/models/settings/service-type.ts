export const ACCOUNTING = 'accounting';
export const AUTHORIZATION_SERVER = 'authorization-server';
export const COMMUNICATION_SERVER = 'communication-server';
export const IDENTITY_PROVIDER = 'identity-provider';
export const INFRASTRUCTURE_CONSOLE = 'infrastructure-console';
