import { Get, Controller } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  root() {
    return { message : 'API endpoint for Accounting Server please Make a request.'};
  }

  @Get('info')
  info() {
    return this.appService.info();
  }
}
