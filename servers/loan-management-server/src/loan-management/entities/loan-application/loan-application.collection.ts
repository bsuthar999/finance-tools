import { Entity, BaseEntity, Column, ObjectIdColumn, ObjectID } from 'typeorm';
import * as uuidv4 from 'uuid/v4';

@Entity()
export class LoanApplication extends BaseEntity {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    uuid: string;

    @Column()
    date: Date;

    @Column()
    createdBy: string;

    @Column(type => ApplicantDetails)
    applicantDetails: ApplicantDetails;

    @Column(type => VehicleInformation)
    vehicleInformation: VehicleInformation;

    @Column(type => LoanDetails)
    loanDetails: LoanDetails;

    constructor() {
        super();
        if (!this.uuid) { this.uuid = uuidv4(); }
      }
}

@Entity()
export class ApplicantDetails {
    @Column()
    name: string;

    @Column()
    address: string;

    @Column()
    entity: string;

    @Column(type => ApplicantIdentity)
    applicantIdentity: ApplicantIdentity;

    @Column()
    pan: string;

    @Column()
    financialDetails: string;

    @Column()
    profession: string;
}

@Entity()
export class ApplicantIdentity {
    @Column()
    identityType: string;

    @Column()
    identityNumber: string;
}

@Entity()
export class VehicleInformation {
    @Column()
    vehicle: string;

    @Column()
    ownersName: string;

    @Column()
    manufacturingYear: Date;

    @Column()
    registrationDate: Date;

    @Column()
    vehicleType: string;

    @Column()
    vehicleVariant: string;

    @Column()
    invoiceNumber: string;

    @Column()
    invoiceAmount: string;

    @Column()
    engineNumber: string;

    @Column()
    vehicleColor: string;

    @Column()
    rcExpiryDate: Date;

    @Column()
    photoUrl: string;
}

@Entity()
export class LoanDetails {
    @Column()
    loanAmount: string;
}
