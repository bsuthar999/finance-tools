import { Test, TestingModule } from '@nestjs/testing';
import { LoanApplicationAggregatesManagerService } from './loan-application-aggregates-manager.service';

describe('LoanApplicationAggregatesManagerService', () => {
  let service: LoanApplicationAggregatesManagerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LoanApplicationAggregatesManagerService],
    }).compile();

    service = module.get<LoanApplicationAggregatesManagerService>(LoanApplicationAggregatesManagerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
